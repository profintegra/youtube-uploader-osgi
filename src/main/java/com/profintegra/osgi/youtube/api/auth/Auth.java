package com.profintegra.osgi.youtube.api.auth;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.common.collect.Lists;
import com.profintegra.osgi.youtube.api.auth.store.DataStore2;
import com.profintegra.osgi.youtube.api.auth.store.FileDataStoreFactory2;
import com.profintegra.osgi.youtube.api.auth.store.StoredCredential2;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Shared class used by every sample. Contains methods for authorizing a user and caching credentials.
 */
public class Auth {
    
    private static final Logger log = LoggerFactory.getLogger(Auth.class); 

    /**
     * Define a global instance of the HTTP transport.
     */
    public static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

    /**
     * Define a global instance of the JSON factory.
     */
    public static final JsonFactory JSON_FACTORY = new JacksonFactory();
    

    /**
     * This is the directory that will be used under the user's home directory where OAuth tokens will be stored.
     */
    private static final String CREDENTIALS_DIRECTORY = ".oauth-credentials";
    private static final String SECRETS_FILE_NAME = "client_secrets.json";
    private static final String CRED_DATA_STORE = "uploadvideo";
    
    private static final String SCOPE_YOUTUBE_UPLOAD = "https://www.googleapis.com/auth/youtube.upload";
    private static final List<String> scopes = Lists.newArrayList(SCOPE_YOUTUBE_UPLOAD);
    
    //public static Credential authorize(List<String> scopes, String credentialDatastore) throws Exception {
    public static Credential authorize(String pSecretsHome) throws Exception {

        File secretsHome = new File(pSecretsHome);
        if(!secretsHome.exists()){ throw new Exception("Provided path to secretsHome directory '"+pSecretsHome+"' is not found");}
        if(!secretsHome.isDirectory()){ throw new Exception("Provided secretsHome have to be a directory: '"+pSecretsHome+"'");}
        if(!secretsHome.canRead()){ throw new Exception("Cannot read the  secretsHome directory : '"+pSecretsHome+"'");}
        
        File secretsFile = new File(secretsHome+File.separator+SECRETS_FILE_NAME);
        if(!secretsFile.exists()){ throw new Exception("'"+SECRETS_FILE_NAME+"' file should exist in the '"+pSecretsHome+"' folder");}
        if(!secretsFile.canRead()){ throw new Exception("Cannot read from '"+pSecretsHome+File.separator+SECRETS_FILE_NAME+"' file. Check file ACLs ");}
  
        // Load client secrets.
        Reader clientSecretReader = new InputStreamReader(new FileInputStream(secretsFile));
        ClientSecrets clientSecrets = ClientSecrets.load(JSON_FACTORY, clientSecretReader);

        // This creates the credentials datastore at ~/.oauth-credentials/${credentialDatastore}
        File credentialsDirectory = new File(pSecretsHome+"/"+CREDENTIALS_DIRECTORY);
        FileDataStoreFactory2 fileDataStoreFactory = new FileDataStoreFactory2(credentialsDirectory);
        DataStore2<StoredCredential2> datastore = fileDataStoreFactory.getDataStore(CRED_DATA_STORE);

        AuthorizationCodeFlow2 flow = new AuthorizationCodeFlow2.Builder(
                HTTP_TRANSPORT, 
                JSON_FACTORY, 
                clientSecrets, 
                scopes).setCredentialDataStore(datastore).build();
        
        String userId = "user";
        
        Credential credential = flow.loadCredential(userId);
        
        if (credential != null
            && (credential.getRefreshToken() != null || credential.getExpiresInSeconds() > 60)) {
          return credential;
        }
        
/*
https://accounts.google.com/o/oauth2/auth?client_id=602445310799-9jq458jr6lq6q4c8vuv062o1tstsrrqa.apps.googleusercontent.com&redirect_uri=http://localhost:50599/Callback&response_type=code&scope=https://www.googleapis.com/auth/youtube.upload        
        
        
        */        
        
        String code = clientSecrets.getDetails().getClientCode(); // "4/xJdNlJrNsYScB_in6hiR8Bv1AWf7ZTA_QdSFbr2JhzY#";
        String redirectUri = clientSecrets.getDetails().getRedirectUris().get(0);

        
        TokenResponse response = flow.newTokenRequest(code).setRedirectUri(redirectUri).execute();
        
        return flow.createAndStoreCredential(response, userId);

    }
}

        
        //Credential credential = flow.loadCredential(userId);
        //return credential;

        // Build the local server and bind it to port 8080
        //LocalServerReceiver localReceiver = new LocalServerReceiver.Builder().setPort(8080).build();
        //LocalServerReceiver localReceiver = new LocalServerReceiver();

        // Authorize.
        //return new AuthorizationCodeInstalledApp(flow, localReceiver).authorize(userId);