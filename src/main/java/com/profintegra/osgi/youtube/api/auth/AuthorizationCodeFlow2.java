package com.profintegra.osgi.youtube.api.auth;


import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.ClientParametersAuthentication;
import com.google.api.client.auth.oauth2.Credential.AccessMethod;
import com.google.api.client.auth.oauth2.CredentialRefreshListener;
import com.google.api.client.auth.oauth2.CredentialStore;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleOAuthConstants;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpExecuteInterceptor;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.Beta;
import com.google.api.client.util.Clock;
import com.google.api.client.util.Preconditions;
import com.profintegra.osgi.youtube.api.auth.store.DataStore2;
import com.profintegra.osgi.youtube.api.auth.store.DataStoreFactory2;
import com.profintegra.osgi.youtube.api.auth.store.StoredCredential2;

import java.io.IOException;
import java.util.Collection;

@SuppressWarnings("deprecation")
public class AuthorizationCodeFlow2 extends AuthorizationCodeFlow1 {

  /**
   * Prompt for consent behavior ({@code "auto"} to request auto-approval or {@code "force"} to
   * force the approval UI to show) or {@code null} for the default behavior.
   */
  private final String approvalPrompt;

  /**
   * Access type ({@code "online"} to request online access or {@code "offline"} to request offline
   * access) or {@code null} for the default behavior.
   */
  private final String accessType;

  /**
   * @param transport HTTP transport
   * @param jsonFactory JSON factory
   * @param clientId client identifier
   * @param clientSecret client secret
   * @param scopes collection of scopes to be joined by a space separator
   *
   * @since 1.15
   */
  public AuthorizationCodeFlow2(HttpTransport transport, JsonFactory jsonFactory,
      String clientId, String clientSecret, Collection<String> scopes) {
    this(new Builder(transport, jsonFactory, clientId, clientSecret, scopes));
  }

  /**
   * @param builder Google authorization code flow builder
   *
   * @since 1.14
   */
  protected AuthorizationCodeFlow2(Builder builder) {
    super(builder);
    accessType = builder.accessType;
    approvalPrompt = builder.approvalPrompt;
  }

  @Override
  public GoogleAuthorizationCodeTokenRequest newTokenRequest(String authorizationCode) {
    // don't need to specify clientId & clientSecret because specifying clientAuthentication
    // don't want to specify redirectUri to give control of it to user of this class
    return new GoogleAuthorizationCodeTokenRequest(getTransport(), getJsonFactory(),
        getTokenServerEncodedUrl(), "", "", authorizationCode, "").setClientAuthentication(
        getClientAuthentication())
        .setRequestInitializer(getRequestInitializer()).setScopes(getScopes());
  }

  @Override
  public GoogleAuthorizationCodeRequestUrl newAuthorizationUrl() {
    // don't want to specify redirectUri to give control of it to user of this class
    return new GoogleAuthorizationCodeRequestUrl(
        getAuthorizationServerEncodedUrl(), getClientId(), "", getScopes()).setAccessType(
        accessType).setApprovalPrompt(approvalPrompt);
  }

  /**
   * Returns the approval prompt behavior ({@code "auto"} to request auto-approval or
   * {@code "force"} to force the approval UI to show) or {@code null} for the default behavior of
   * {@code "auto"}.
   */
  public final String getApprovalPrompt() {
    return approvalPrompt;
  }

  /**
   * Returns the access type ({@code "online"} to request online access or {@code "offline"} to
   * request offline access) or {@code null} for the default behavior of {@code "online"}.
   */
  public final String getAccessType() {
    return accessType;
  }

  /**
   * Google authorization code flow builder.
   *
   * <p>
   * Implementation is not thread-safe.
   * </p>
   */
  public static class Builder extends AuthorizationCodeFlow1.Builder {

    /**
     * Prompt for consent behavior ({@code "auto"} to request auto-approval or {@code "force"} to
     * force the approval UI to show) or {@code null} for the default behavior.
     */
    String approvalPrompt;

    /**
     * Access type ({@code "online"} to request online access or {@code "offline"} to request
     * offline access) or {@code null} for the default behavior.
     */
    String accessType;

    /**
     *
     * @param transport HTTP transport
     * @param jsonFactory JSON factory
     * @param clientId client identifier
     * @param clientSecret client secret
     * @param scopes collection of scopes to be joined by a space separator (or a single value
     *        containing multiple space-separated scopes)
     *
     * @since 1.15
     */
    public Builder(HttpTransport transport, JsonFactory jsonFactory, String clientId,
        String clientSecret, Collection<String> scopes) {
      super(BearerToken.authorizationHeaderAccessMethod(), transport, jsonFactory, new GenericUrl(
          GoogleOAuthConstants.TOKEN_SERVER_URL), new ClientParametersAuthentication(
          clientId, clientSecret), clientId, GoogleOAuthConstants.AUTHORIZATION_SERVER_URL);
      setScopes(scopes);
    }

    /**
     * @param transport HTTP transport
     * @param jsonFactory JSON factory
     * @param clientSecrets Google client secrets
     * @param scopes collection of scopes to be joined by a space separator
     *
     * @since 1.15
     */
    public Builder(HttpTransport transport, JsonFactory jsonFactory,
        ClientSecrets clientSecrets, Collection<String> scopes) {
      super(BearerToken.authorizationHeaderAccessMethod(), transport, jsonFactory, new GenericUrl(
          GoogleOAuthConstants.TOKEN_SERVER_URL), new ClientParametersAuthentication(
          clientSecrets.getDetails().getClientId(), clientSecrets.getDetails().getClientSecret()),
          clientSecrets.getDetails().getClientId(), GoogleOAuthConstants.AUTHORIZATION_SERVER_URL);
      setScopes(scopes);
    }

    @Override
    public AuthorizationCodeFlow2 build() {
      return new AuthorizationCodeFlow2(this);
    }

    @Override
    public Builder setDataStoreFactory(DataStoreFactory2 dataStore) throws IOException {
      return (Builder) super.setDataStoreFactory(dataStore);
    }

    public Builder setCredentialDataStore(DataStore2<StoredCredential2> typedDataStore) {
      return (Builder) super.setCredentialDataStore1(typedDataStore);
    }
      

    @Override
    public Builder setCredentialCreatedListener(
        AuthorizationCodeFlow2.CredentialCreatedListener credentialCreatedListener) {
      return (Builder) super.setCredentialCreatedListener(credentialCreatedListener);
    }

    @Beta
    @Override
    @Deprecated
    public Builder setCredentialStore(CredentialStore credentialStore) {
      return (Builder) super.setCredentialStore(credentialStore);
    }

    @Override
    public Builder setRequestInitializer(HttpRequestInitializer requestInitializer) {
      return (Builder) super.setRequestInitializer(requestInitializer);
    }

    @Override
    public Builder setScopes(Collection<String> scopes) {
      Preconditions.checkState(!scopes.isEmpty());
      return (Builder) super.setScopes(scopes);
    }

    /**
     * @since 1.11
     */
    @Override
    public Builder setMethod(AccessMethod method) {
      return (Builder) super.setMethod(method);
    }

    /**
     * @since 1.11
     */
    @Override
    public Builder setTransport(HttpTransport transport) {
      return (Builder) super.setTransport(transport);
    }

    /**
     * @since 1.11
     */
    @Override
    public Builder setJsonFactory(JsonFactory jsonFactory) {
      return (Builder) super.setJsonFactory(jsonFactory);
    }

    /**
     * @since 1.11
     */
    @Override
    public Builder setTokenServerUrl(GenericUrl tokenServerUrl) {
      return (Builder) super.setTokenServerUrl(tokenServerUrl);
    }

    /**
     * @since 1.11
     */
    @Override
    public Builder setClientAuthentication(HttpExecuteInterceptor clientAuthentication) {
      return (Builder) super.setClientAuthentication(clientAuthentication);
    }

    /**
     * @since 1.11
     */
    @Override
    public Builder setClientId(String clientId) {
      return (Builder) super.setClientId(clientId);
    }

    /**
     * @since 1.11
     */
    @Override
    public Builder setAuthorizationServerEncodedUrl(String authorizationServerEncodedUrl) {
      return (Builder) super.setAuthorizationServerEncodedUrl(authorizationServerEncodedUrl);
    }

    /**
     * @since 1.11
     */
    @Override
    public Builder setClock(Clock clock) {
      return (Builder) super.setClock(clock);
    }

    @Override
    public Builder addRefreshListener(CredentialRefreshListener refreshListener) {
      return (Builder) super.addRefreshListener(refreshListener);
    }

    @Override
    public Builder setRefreshListeners(Collection<CredentialRefreshListener> refreshListeners) {
      return (Builder) super.setRefreshListeners(refreshListeners);
    }

    /**
     * Sets the approval prompt behavior ({@code "auto"} to request auto-approval or {@code "force"}
     * to force the approval UI to show) or {@code null} for the default behavior ({@code "auto"}
     * for web applications and {@code "force"} for installed applications).
     *
     * <p>
     * By default this has the value {@code null}.
     * </p>
     *
     * <p>
     * Overriding is only supported for the purpose of calling the super implementation and changing
     * the return type, but nothing else.
     * </p>
     */
    public Builder setApprovalPrompt(String approvalPrompt) {
      this.approvalPrompt = approvalPrompt;
      return this;
    }

    /**
     * Returns the approval prompt behavior ({@code "auto"} to request auto-approval or
     * {@code "force"} to force the approval UI to show) or {@code null} for the default behavior of
     * {@code "auto"}.
     */
    public final String getApprovalPrompt() {
      return approvalPrompt;
    }

    /**
     * Sets the access type ({@code "online"} to request online access or {@code "offline"} to
     * request offline access) or {@code null} for the default behavior ({@code "online"} for web
     * applications and {@code "offline"} for installed applications).
     *
     * <p>
     * By default this has the value {@code null}.
     * </p>
     *
     * <p>
     * Overriding is only supported for the purpose of calling the super implementation and changing
     * the return type, but nothing else.
     * </p>
     */
    public Builder setAccessType(String accessType) {
      this.accessType = accessType;
      return this;
    }

    /**
     * Returns the access type ({@code "online"} to request online access or {@code "offline"} to
     * request offline access) or {@code null} for the default behavior of {@code "online"}.
     */
    public final String getAccessType() {
      return accessType;
    }
  }
}
