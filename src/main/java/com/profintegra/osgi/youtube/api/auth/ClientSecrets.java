package com.profintegra.osgi.youtube.api.auth;

import com.google.api.client.json.GenericJson;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.Key;
import com.google.api.client.util.Preconditions;

import java.io.IOException;
import java.io.Reader;
import java.util.List;


public final class ClientSecrets extends GenericJson {

  /** Details for installed applications. */
  @Key
  private Details installed;

  /** Details for web applications. */
  @Key
  private Details web;

  /** Returns the details for installed applications. */
  public Details getInstalled() {
    return installed;
  }

  /** Sets the details for installed applications. */
  public ClientSecrets setInstalled(Details installed) {
    this.installed = installed;
    return this;
  }

  /** Returns the details for web applications. */
  public Details getWeb() {
    return web;
  }

  /** Sets the details for web applications. */
  public ClientSecrets setWeb(Details web) {
    this.web = web;
    return this;
  }

  /** Returns the details for either installed or web applications. */
  public Details getDetails() {
    // that web or installed, but not both
    Preconditions.checkArgument((web == null) != (installed == null));
    return web == null ? installed : web;
  }

  /** Client credential details. */
  public static final class Details extends GenericJson {

    /** Client ID. */
    @Key("client_id")
    private String clientId;

    /** Client secret. */
    @Key("client_secret")
    private String clientSecret;

    /** Redirect URIs. */
    @Key("redirect_uris")
    private List<String> redirectUris;

    /** Client Scopes (API Access Scopes). */
    @Key("client_scopes")
    private List<String> clientScopes;

    /** Authorization server URI. */
    @Key("auth_uri")
    private String authUri;

    /** Token server URI. */
    @Key("token_uri")
    private String tokenUri;

    /** Code, returned by Authorization server. */
    @Key("client_code")
    private String clientCode;

    /** Returns the client Code. */
    public String getClientCode() {
      return clientCode;
    }

    /** Sets the client Code. */
    public Details getClientCode(String clientCode) {
      this.clientCode = clientCode;
      return this;
    }

    /** Returns the client ID. */
    public String getClientId() {
      return clientId;
    }

    /** Sets the client ID. */
    public Details setClientId(String clientId) {
      this.clientId = clientId;
      return this;
    }

    /** Returns the client secret. */
    public String getClientSecret() {
      return clientSecret;
    }

    /** Sets the client secret. */
    public Details setClientSecret(String clientSecret) {
      this.clientSecret = clientSecret;
      return this;
    }

    /** Returns the redirect URIs. */
    public List<String> getRedirectUris() {
      return redirectUris;
    }

    /** Sets the redirect URIs. */
    public Details setRedirectUris(List<String> redirectUris) {
      this.redirectUris = redirectUris;
      return this;
    }

    /** Returns API Scopes for Client to be authorized to use. */
    public List<String> getClientScopes() {
      return clientScopes;
    }
//scopes
    /** Sets API Scopes for Client to be authorized to use. */
    public Details setClientScopes(List<String> clientScopes) {
      this.clientScopes = clientScopes;
      return this;
    }

    /** Returns the authorization server URI. */
    public String getAuthUri() {
      return authUri;
    }

    /** Sets the authorization server URI. */
    public Details setAuthUri(String authUri) {
      this.authUri = authUri;
      return this;
    }

    /** Returns the token server URI. */
    public String getTokenUri() {
      return tokenUri;
    }

    /** Sets the token server URI. */
    public Details setTokenUri(String tokenUri) {
      this.tokenUri = tokenUri;
      return this;
    }

    @Override
    public Details set(String fieldName, Object value) {
      return (Details) super.set(fieldName, value);
    }

    @Override
    public Details clone() {
      return (Details) super.clone();
    }
  }

  @Override
  public ClientSecrets set(String fieldName, Object value) {
    return (ClientSecrets) super.set(fieldName, value);
  }

  @Override
  public ClientSecrets clone() {
    return (ClientSecrets) super.clone();
  }

  /**
   * Loads the {@code client_secrets.json} file from the given reader.
   *
   * @since 1.15
   */
  public static ClientSecrets load(JsonFactory jsonFactory, Reader reader)
      throws IOException {
    return jsonFactory.fromReader(reader, ClientSecrets.class);
  }
}

