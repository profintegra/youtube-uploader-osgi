package com.profintegra.osgi.youtube.api.auth.store;

import com.google.api.client.util.Beta;
import com.google.api.client.util.Preconditions;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

/**
 * {@link Beta} <br/>
 * Abstract data store implementation.
 *
 * @param <V> serializable type of the mapped value
 *
 * @author Yaniv Inbar
 * @since 1.16
 */
@Beta
public abstract class AbstractDataStore2<V extends Serializable> implements DataStore2<V> {

  /** Data store factory. */
  private final DataStoreFactory2 dataStoreFactory;

  /** Data store ID. */
  private final String id;

  /**
   * @param dataStoreFactory data store factory
   * @param id data store ID
   */
  protected AbstractDataStore2(DataStoreFactory2 dataStoreFactory, String id) {
    this.dataStoreFactory = Preconditions.checkNotNull(dataStoreFactory);
    this.id = Preconditions.checkNotNull(id);
  }

  /**
   * {@inheritDoc}
   *
   * <p>
   * Overriding is only supported for the purpose of calling the super implementation and changing
   * the return type, but nothing else.
   * </p>
   */
  @Override
  public DataStoreFactory2 getDataStoreFactory() {
    return dataStoreFactory;
  }

  @Override
  public final String getId() {
    return id;
  }

  /**
   * {@inheritDoc}
   *
   * <p>
   * Default implementation is to call {@link #get(String)} and check if it is {@code null}.
   * </p>
   */
  @Override
  public boolean containsKey(String key) throws IOException {
    return get(key) != null;
  }

  /**
   * {@inheritDoc}
   *
   * <p>
   * Default implementation is to call {@link Collection#contains(Object)} on {@link #values()}.
   * </p>
   */
  @Override
  public boolean containsValue(V value) throws IOException {
    return values().contains(value);
  }

  /**
   * {@inheritDoc}
   *
   * <p>
   * Default implementation is to check if {@link #size()} is {@code 0}.
   * </p>
   */
  @Override
  public boolean isEmpty() throws IOException {
    return size() == 0;
  }

  /**
   * {@inheritDoc}
   *
   * <p>
   * Default implementation is to call {@link Set#size()} on {@link #keySet()}.
   * </p>
   */
  @Override
  public int size() throws IOException {
    return keySet().size();
  }
}