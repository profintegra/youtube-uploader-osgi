package com.profintegra.osgi.youtube.api.auth.store;


import com.google.api.client.util.Maps;
import com.google.api.client.util.Preconditions;
import com.google.api.client.util.store.DataStore;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;

public abstract class AbstractDataStoreFactory2 implements DataStoreFactory2 {

  /** Lock on access to the data store map. */
  private final Lock lock = new ReentrantLock();

  /** Map of data store ID to data store. */
  private final Map<String, DataStore2<? extends Serializable>> dataStoreMap = Maps.newHashMap();

  /**
   * Pattern to control possible values for the {@code id} parameter of
   * {@link #getDataStore(String)}.
   */
  private static final Pattern ID_PATTERN = Pattern.compile("\\w{1,30}");

  @Override
  public final <V extends Serializable> DataStore2<V> getDataStore(String id) throws IOException {
    Preconditions.checkArgument(
        ID_PATTERN.matcher(id).matches(), "%s does not match pattern %s", id, ID_PATTERN);
    lock.lock();
    try {
      @SuppressWarnings("unchecked")
      DataStore2<V> dataStore = (DataStore2<V>) dataStoreMap.get(id);
      if (dataStore == null) {
        dataStore = createDataStore(id);
        dataStoreMap.put(id, dataStore);
      }
      return dataStore;
    } finally {
      lock.unlock();
    }
  }

  /**
   * Returns a new instance of a type-specific data store based on the given unique ID.
   *
   * <p>
   * The {@link DataStore#getId()} must match the {@code id} parameter from this method.
   * </p>
   *
   * @param id unique ID to refer to typed data store
   * @param <V> serializable type of the mapped value
   */
  protected abstract <V extends Serializable> DataStore2<V> createDataStore(String id)
      throws IOException;
}
