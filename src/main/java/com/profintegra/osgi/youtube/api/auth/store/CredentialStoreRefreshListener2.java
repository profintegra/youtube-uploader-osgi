package com.profintegra.osgi.youtube.api.auth.store;


import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.CredentialRefreshListener;
import com.google.api.client.auth.oauth2.CredentialStore;
import com.google.api.client.auth.oauth2.TokenErrorResponse;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.util.Preconditions;

import java.io.IOException;


public final class CredentialStoreRefreshListener2 implements CredentialRefreshListener {

  /** Credential store. */
  private final CredentialStore credentialStore;

  /** User ID whose credential is to be updated. */
  private final String userId;

  /**
   * @param userId user ID whose credential is to be updated
   * @param credentialStore credential store
   */
  public CredentialStoreRefreshListener2(String userId, CredentialStore credentialStore) {
    this.userId = Preconditions.checkNotNull(userId);
    this.credentialStore = Preconditions.checkNotNull(credentialStore);
  }

  @Override
  public void onTokenResponse(Credential credential, TokenResponse tokenResponse)
      throws IOException {
    makePersistent(credential);
  }

  @Override
  public void onTokenErrorResponse(Credential credential, TokenErrorResponse tokenErrorResponse)
      throws IOException {
    makePersistent(credential);
  }

  /** Returns the credential store. */
  public CredentialStore getCredentialStore() {
    return credentialStore;
  }

  /** Stores the updated credential in the credential store. */
  public void makePersistent(Credential credential) throws IOException {
    credentialStore.store(userId, credential);
  }
}

