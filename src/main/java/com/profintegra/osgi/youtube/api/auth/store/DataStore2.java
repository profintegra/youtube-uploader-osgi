package com.profintegra.osgi.youtube.api.auth.store;


import com.google.api.client.util.Beta;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

/**
 * {@link Beta} <br/>
 * Stores and manages serializable data of a specific type, where the key is a string and the value
 * is a {@link Serializable} object.
 *
 * <p>
 * {@code null} keys or values are not allowed. Implementation should be thread-safe.
 * </p>
 *
 * @param <V> serializable type of the mapped value
 *
 * @author Yaniv Inbar
 * @since 1.16
 */
@Beta
public interface DataStore2<V extends Serializable> {

  /** Returns the data store factory. */
  DataStoreFactory2 getDataStoreFactory();

  /** Returns the data store ID. */
  String getId();

  /** Returns the number of stored keys. */
  int size() throws IOException;

  /** Returns whether there are any stored keys. */
  boolean isEmpty() throws IOException;

  /** Returns whether the store contains the given key. */
  boolean containsKey(String key) throws IOException;

  /** Returns whether the store contains the given value. */
  boolean containsValue(V value) throws IOException;

  /**
   * Returns the unmodifiable set of all stored keys.
   *
   * <p>
   * Order of the keys is not specified.
   * </p>
   */
  Set<String> keySet() throws IOException;

  /** Returns the unmodifiable collection of all stored values. */
  Collection<V> values() throws IOException;

  /**
   * Returns the stored value for the given key or {@code null} if not found.
   *
   * @param key key or {@code null} for {@code null} result
   */
  V get(String key) throws IOException;

  /**
   * Stores the given value for the given key (replacing any existing value).
   *
   * @param key key
   * @param value value object
   */
  DataStore2<V> set(String key, V value) throws IOException;

  /** Deletes all of the stored keys and values. */
  DataStore2<V> clear() throws IOException;

  /**
   * Deletes the stored key and value based on the given key, or ignored if the key doesn't already
   * exist.
   *
   * @param key key or {@code null} to ignore
   */
  DataStore2<V> delete(String key) throws IOException;

  // TODO(yanivi): implement entrySet()?
}
