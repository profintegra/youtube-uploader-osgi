package com.profintegra.osgi.youtube.api.auth.store;


import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.CredentialRefreshListener;
import com.google.api.client.auth.oauth2.TokenErrorResponse;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.util.Preconditions;

import java.io.IOException;


public final class DataStoreCredentialRefreshListener2 implements CredentialRefreshListener {

  /** Stored credential data store. */
  private final DataStore2<StoredCredential2> credentialDataStore;

  /** User ID whose credential is to be updated. */
  private final String userId;


  public DataStoreCredentialRefreshListener2(String userId, DataStore2<StoredCredential2> credentialDataStore) {
    this.userId = Preconditions.checkNotNull(userId);
    this.credentialDataStore = Preconditions.checkNotNull(credentialDataStore);
  }

  public void onTokenResponse(Credential credential, TokenResponse tokenResponse)
      throws IOException {
    makePersistent(credential);
  }

  public void onTokenErrorResponse(Credential credential, TokenErrorResponse tokenErrorResponse)
      throws IOException {
    makePersistent(credential);
  }

  /** Returns the stored credential data store. */
  public DataStore2<StoredCredential2> getCredentialDataStore() {
    return credentialDataStore;
  }

  /** Stores the updated credential in the credential store. */
  public void makePersistent(Credential credential) throws IOException {
    credentialDataStore.set(userId, new StoredCredential2(credential));
  }
}

