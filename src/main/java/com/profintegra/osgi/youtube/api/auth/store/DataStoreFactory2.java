package com.profintegra.osgi.youtube.api.auth.store;


import com.google.api.client.util.Beta;

import java.io.IOException;
import java.io.Serializable;


@Beta
public interface DataStoreFactory2 {


  <V extends Serializable> DataStore2<V> getDataStore(String id) throws IOException;
}
