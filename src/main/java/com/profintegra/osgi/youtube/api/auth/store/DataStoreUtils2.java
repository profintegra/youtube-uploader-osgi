package com.profintegra.osgi.youtube.api.auth.store;

import java.io.IOException;


public final class DataStoreUtils2 {


  public static String toString(DataStore2<?> dataStore) {
    try {
      StringBuilder sb = new StringBuilder();
      sb.append('{');
      boolean first = true;
      for (String key : dataStore.keySet()) {
        if (first) {
          first = false;
        } else {
          sb.append(", ");
        }
        sb.append(key).append('=').append(dataStore.get(key));
      }
      return sb.append('}').toString();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private DataStoreUtils2() {
  }
}

