package com.profintegra.osgi.youtube.api.auth.store;


import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.util.Objects;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public final class StoredCredential2 implements Serializable {

  /** Default data store ID. */
  public static final String DEFAULT_DATA_STORE_ID = StoredCredential2.class.getSimpleName();

  private static final long serialVersionUID = 1L;

  /** Lock on access to the store. */
  private final Lock lock = new ReentrantLock();

  /** Access token or {@code null} for none. */
  private String accessToken;

  /** Expected expiration time in milliseconds or {@code null} for none. */
  private Long expirationTimeMilliseconds;

  /** Refresh token or {@code null} for none. */
  private String refreshToken;

  public StoredCredential2() {
  }

  /**
   * @param credential existing credential to copy from
   */
  public StoredCredential2(Credential credential) {
    setAccessToken(credential.getAccessToken());
    setRefreshToken(credential.getRefreshToken());
    setExpirationTimeMilliseconds(credential.getExpirationTimeMilliseconds());
  }

  /** Returns the access token or {@code null} for none. */
  public String getAccessToken() {
    lock.lock();
    try {
      return accessToken;
    } finally {
      lock.unlock();
    }
  }

  /** Sets the access token or {@code null} for none. */
  public StoredCredential2 setAccessToken(String accessToken) {
    lock.lock();
    try {
      this.accessToken = accessToken;
    } finally {
      lock.unlock();
    }
    return this;
  }

  /** Returns the expected expiration time in milliseconds or {@code null} for none. */
  public Long getExpirationTimeMilliseconds() {
    lock.lock();
    try {
      return expirationTimeMilliseconds;
    } finally {
      lock.unlock();
    }
  }

  /** Sets the expected expiration time in milliseconds or {@code null} for none. */
  public StoredCredential2 setExpirationTimeMilliseconds(Long expirationTimeMilliseconds) {
    lock.lock();
    try {
      this.expirationTimeMilliseconds = expirationTimeMilliseconds;
    } finally {
      lock.unlock();
    }
    return this;
  }

  /** Returns the refresh token or {@code null} for none. */
  public String getRefreshToken() {
    lock.lock();
    try {
      return refreshToken;
    } finally {
      lock.unlock();
    }
  }

  /** Sets the refresh token or {@code null} for none. */
  public StoredCredential2 setRefreshToken(String refreshToken) {
    lock.lock();
    try {
      this.refreshToken = refreshToken;
    } finally {
      lock.unlock();
    }
    return this;
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(StoredCredential2.class)
        .add("accessToken", getAccessToken())
        .add("refreshToken", getRefreshToken())
        .add("expirationTimeMilliseconds", getExpirationTimeMilliseconds())
        .toString();
  }

  @Override
  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof StoredCredential2)) {
      return false;
    }
    StoredCredential2 o = (StoredCredential2) other;
    return Objects.equal(getAccessToken(), o.getAccessToken())
        && Objects.equal(getRefreshToken(), o.getRefreshToken())
        && Objects.equal(getExpirationTimeMilliseconds(), o.getExpirationTimeMilliseconds());
  }

  @Override
  public int hashCode() {
    return Arrays.hashCode(
        new Object[] {getAccessToken(), getRefreshToken(), getExpirationTimeMilliseconds()});
  }

  /**
   * Returns the stored credential data store using the ID {@link #DEFAULT_DATA_STORE_ID}.
   *
   * @param dataStoreFactory data store factory
   * @return stored credential data store
   */
  public static DataStore2<StoredCredential2> getDefaultDataStore(DataStoreFactory2 dataStoreFactory)
      throws IOException {
    return dataStoreFactory.getDataStore(DEFAULT_DATA_STORE_ID);
  }
}
