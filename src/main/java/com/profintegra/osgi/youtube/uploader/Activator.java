package com.profintegra.osgi.youtube.uploader;

//import com.profintegra.osgi.youtube.uploader.impl.YouTubeVideoUploader;
import com.profintegra.osgi.youtube.uploader.camel.YouTubeUploaderOsgiCamelContext;
import com.profintegra.osgi.youtube.uploader.camel.routes.YouTubeUploaderRoute;
import org.apache.camel.impl.DefaultCamelContext;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Activator implements BundleActivator {

    private static final Logger log = LoggerFactory.getLogger(Activator.class);
    private String apiID = null;
    private DefaultCamelContext camelContext = null;
    
    @Override
    public void start(BundleContext bundleContext) throws Exception {
	
	apiID = bundleContext.getBundle().getSymbolicName();
        log.info(apiID+". Start requested ");
        
        if(camelContext != null && camelContext.isStarted()){
            log.info(apiID+" OSGi Bundle is already started");
            return;
        }        
	
	camelContext = new YouTubeUploaderOsgiCamelContext(bundleContext);
	camelContext.start();
	
        log.info(apiID+" OSGi Bundle is started");
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        
        log.info(apiID+". Stop requested ");
        
        if(camelContext != null && !camelContext.isStopped()){
            camelContext.stop();
        }
        
        log.info(apiID+" OSGi Bundle is stoped");
    }
}
