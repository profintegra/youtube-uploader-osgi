package com.profintegra.osgi.youtube.uploader;


import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.http.InputStreamContent;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTube.Videos.Insert;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoSnippet;
import com.google.api.services.youtube.model.VideoStatus;
import com.google.common.collect.Lists;
import com.profintegra.osgi.youtube.api.auth.Auth;
import com.profintegra.osgi.youtube.uploader.internal.UploadProgressListener;
import java.io.File;
import java.io.FileInputStream;
import org.apache.camel.Exchange;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Upload a video to the authenticated user's channel. Use OAuth 2.0 to
 authorize the request. Note that you must add your video files to the
 project folder to _upload them with this application.
 *
 * @author Jeremy Walker
 */
public class YouTubeVideoUploader {
    
    private static final Logger log = LoggerFactory.getLogger(YouTubeVideoUploader.class); 

    private static YouTube youtube = null;
    private static final String VIDEO_FILE_FORMAT = "video/*";
    
    private final String PRIVACY_STATUS_DEFAULT = "private";
    private String VIDEO_TITLE_DEFAULT = "Video from VizOne ";
    private String VIDEO_DESCR_DEFAULT = "Video uploaded from VizOne ";
    
    
    
    private final String APP_NAME = "youtube-uploadvideo";
    private  String AUTH_HOME_FOLDER = "/etc/google/oauth";
    
    public YouTubeVideoUploader(){}
    
    public YouTubeVideoUploader(String authHomefolder){
        AUTH_HOME_FOLDER = authHomefolder;
    }
    
    
    private void _initYouTubeClient() throws Exception{
        
        Calendar cal = Calendar.getInstance();
        VIDEO_TITLE_DEFAULT = "Video from VizOne " + cal.getTime();
        VIDEO_DESCR_DEFAULT = "Video uploaded from VizOne " + "on " + cal.getTime();        
        
        if(youtube != null){ return;}

            youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, 
                                          Auth.JSON_FACTORY, 
                                          Auth.authorize(AUTH_HOME_FOLDER))
                        .setApplicationName(APP_NAME)
                        .build();
    }
    
    private Insert _prepareVideoInsert(final Map<String,String> params,
                                                      final List<String> tags) throws Exception{
        if(youtube == null){ throw new Exception("YuoTube service is NULL. Cannot proceed.");}
        if(params == null){ throw new Exception("Required parameters have not been provided");}
        String fileName = params.get("fileName");
        if(fileName == null || fileName.isEmpty()){ 
            throw new Exception("Required parameter 'fileName' has not been provided");
        }
        FileInputStream fi = new FileInputStream(fileName);
        InputStreamContent mediaContent = new InputStreamContent(VIDEO_FILE_FORMAT, fi);
        
        String privacyStatus = params.get("privacyStatus");
        String title = params.get("title");
        String descr = params.get("descr");

        VideoStatus status = new VideoStatus();
        status.setPrivacyStatus(privacyStatus != null?privacyStatus:PRIVACY_STATUS_DEFAULT);

        // Most of the video's metadata is set on the VideoSnippet object.
        VideoSnippet snippet = new VideoSnippet();
        snippet.setTitle(title != null?title:VIDEO_TITLE_DEFAULT);
        snippet.setDescription(descr != null?descr:VIDEO_DESCR_DEFAULT);
        if(tags != null && tags.size() > 0){snippet.setTags(tags);}

        Video videoObjectDefiningMetadata = new Video();
        videoObjectDefiningMetadata.setStatus(status);
        videoObjectDefiningMetadata.setSnippet(snippet);

        // Insert the video. The command sends three arguments. The first
        // specifies which information the API request is setting and which
        // information the API response should return. The second argument
        // is the video resource that contains metadata about the new video.
        // The third argument is the actual video content.
        return youtube.videos().insert("snippet,statistics,status", 
                                       videoObjectDefiningMetadata, 
                                       mediaContent);
    }

    private Video _executeUpload(Insert videoInsert) throws Exception{

        MediaHttpUploader uploader = videoInsert.getMediaHttpUploader();
        uploader.setDirectUploadEnabled(false);
        uploader.setProgressListener(new UploadProgressListener());

      return videoInsert.execute();
    }

    public void upload(Exchange exchange){
        
        String localFileHeader = "CamelFileLocalWorkPath";
        
        //String payload = exchange.getIn().getBody(String.class);
        Map<String, Object> headers =   exchange.getIn().getHeaders();
        log.info("YouTubeUploader->upload:");
        //log.info(payload);
        String localPath = (String)headers.get(localFileHeader);
        File file = new File(localPath);
        log.info("localFile: "+localPath);
        log.info("exists?: "+file.exists());
        
        if(!file.exists() || !file.canRead()){
            log.info("File '"+file.getAbsolutePath()+"' doesnt exist or not readable. Cannot proceed with upload. Terminating...");
            return;
        }
        
//        for(String key : headers.keySet()){
//            log.info("Header "+key+" : "+headers.get(key));
//        }

            Map<String,String> params = new HashMap<>();
            params.put("privacyStatus",(String)headers.get("privacyStatus"));
            params.put("title", (String)headers.get("title"));
            params.put("descr", (String)headers.get("descr"));
            params.put("fileName", file.getAbsolutePath());
            
            List<String> tags = new ArrayList<>();
            tags.add("VizOne");       
        
        
        _upload(params, tags);
        
        //return exchange;
        
    }
    public void _upload(Map<String,String> params, List<String> tags){
    
        try {
            
            _initYouTubeClient();

            log.info("Uploading: " + params.get("fileName"));
            
            Insert videoInsert =  _prepareVideoInsert(params, tags);

            // Call the API and _upload the video.
            Video returnedVideo = _executeUpload(videoInsert);

            // Print data about the newly inserted video from the API response.
            log.info("\n================== Returned Video ==================\n");
            log.info("  - Id: " + returnedVideo.getId());
            log.info("  - Title: " + returnedVideo.getSnippet().getTitle());
            log.info("  - Tags: " + returnedVideo.getSnippet().getTags());
            log.info("  - Privacy Status: " + returnedVideo.getStatus().getPrivacyStatus());
            log.info("  - Video Count: " + returnedVideo.getStatistics().getViewCount());


        } catch (GoogleJsonResponseException e) {
            log.error("GoogleJsonResponseException code: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());
            log.error("Details",e);
        } catch (IOException e) {
            log.error("IOException: " + e.getMessage());
            log.error("Details",e);
        } catch (Throwable t) {
            log.error("Throwable: " + t.getMessage());
            log.error("Details",t);
        } 

    }
}
