package com.profintegra.osgi.youtube.uploader.camel;

import com.profintegra.osgi.youtube.uploader.camel.routes.YouTubeUploaderRoute;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.spi.Registry;
import org.osgi.framework.BundleContext;


public class YouTubeUploaderOsgiCamelContext extends DefaultCamelContext{
//public class YouTubeUploaderOsgiCamelContext extends OsgiDefaultCamelContext{

    public YouTubeUploaderOsgiCamelContext(BundleContext bundleContext, Registry registry) throws Exception {
        super(registry);
        this.setAutoStartup(Boolean.TRUE);
        addDefaultRoutes();
    }

    public YouTubeUploaderOsgiCamelContext(BundleContext bundleContext)  throws Exception {
        //this(bundleContext, new OsgiServiceRegistry(bundleContext));
        super();
        this.setAutoStartup(Boolean.TRUE);
        addDefaultRoutes();        
    }
    
    private void addDefaultRoutes() throws Exception{
        super.addRoutes(new YouTubeUploaderRoute(this));    
    }
}
