package com.profintegra.osgi.youtube.uploader.camel.routes;

import com.profintegra.osgi.youtube.uploader.YouTubeVideoUploader;
import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;

public class YouTubeUploaderRoute extends RouteBuilder {
    
    public String YUOTUBE_UPLOADER_CAMEL_ROUTE_ID = "route-youtube-uploader";
    public String YUOTUBE_UPLOADER_CAMEL_ROUTE_START = "direct-vm:youtube-uploader-start";
    
    @Override
    public void configure() throws Exception {
        from(YUOTUBE_UPLOADER_CAMEL_ROUTE_START).routeId(YUOTUBE_UPLOADER_CAMEL_ROUTE_ID).
                bean(new YouTubeVideoUploader(), "upload");
    }


    public YouTubeUploaderRoute(CamelContext context) {
        super(context);
    }

    public YouTubeUploaderRoute(String routeId, CamelContext context) {
        super(context);
        if(routeId != null && !routeId.isEmpty()){ 
            YUOTUBE_UPLOADER_CAMEL_ROUTE_ID = routeId;
        }
    }
    
    
}
