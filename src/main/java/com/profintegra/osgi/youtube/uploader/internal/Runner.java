
package com.profintegra.osgi.youtube.uploader.internal;

import com.profintegra.osgi.youtube.uploader.YouTubeVideoUploader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Runner {
    
    public static void main(String[] args){
    
        YouTubeVideoUploader uploader = new YouTubeVideoUploader("H:\\oauth");
        
            Map<String,String> params = new HashMap<>();
            params.put("privacyStatus", "public");
            params.put("title", "Video title");
            params.put("descr", "Video descr");
            params.put("fileName", "/tmp/test.mp4");
            
            List<String> tags = new ArrayList<>();
            tags.add("test");
            tags.add("example");
            tags.add("java");
            tags.add("YouTube Data API V3");
            tags.add("erase me");         
        
        uploader._upload(params, tags);        
        
    }
    
}
