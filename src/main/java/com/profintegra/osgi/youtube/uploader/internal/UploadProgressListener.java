package com.profintegra.osgi.youtube.uploader.internal;

import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class UploadProgressListener implements MediaHttpUploaderProgressListener{
    
    private static final Logger log = LoggerFactory.getLogger(UploadProgressListener.class);
    
                @Override
                public void progressChanged(MediaHttpUploader uploader) throws IOException {
                    switch (uploader.getUploadState()) {
                        case INITIATION_STARTED:
                            log.info("Initiation Started");
                            break;
                        case INITIATION_COMPLETE:
                            log.info("Initiation Completed");
                            break;
                        case MEDIA_IN_PROGRESS:
                            log.info("Upload in progress");
                            long p = 0;
                            try{ p = uploader. getNumBytesUploaded();}catch(Exception e){
                                log.info("Warn: " + e.getMessage());
                            }
                            log.info("Uploaded bytes: " + p);
                            break;
                        case MEDIA_COMPLETE:
                            log.info("Upload Completed!");
                            break;
                        case NOT_STARTED:
                            log.info("Upload Not Started!");
                            break;
                    }
                }    
}
